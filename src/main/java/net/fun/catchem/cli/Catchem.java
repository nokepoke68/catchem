package net.fun.catchem.cli;

import POGOProtos.Inventory.Item.ItemAwardOuterClass;
import POGOProtos.Inventory.Item.ItemIdOuterClass;
import com.pokegoapi.api.PokemonGo;
import com.pokegoapi.api.inventory.Inventories;
import com.pokegoapi.api.inventory.Item;
import com.pokegoapi.api.inventory.ItemBag;
import com.pokegoapi.api.map.Point;
import com.pokegoapi.api.map.fort.Pokestop;
import com.pokegoapi.api.map.fort.PokestopLootResult;
import com.pokegoapi.api.player.PlayerProfile;
import com.pokegoapi.api.pokemon.Pokemon;
import com.pokegoapi.exceptions.InvalidCurrencyException;
import com.pokegoapi.exceptions.LoginFailedException;
import com.pokegoapi.exceptions.NoSuchItemException;
import com.pokegoapi.exceptions.RemoteServerException;
import net.fun.catchem.auth.GoogleAuthenticator;
import net.fun.catchem.geo.GeoUtils;
import net.fun.catchem.geo.Walker;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Catchem {
    private static final int SPIRAL_STEPS = 20;     // number of "corners" in rectangular spiral search
    private static final int STEP_SIZE = 20;        // 20 meter steps
    private static final int TELEPORT_STEP = 50;    // 50 meter jumps
    private static Map<ItemIdOuterClass.ItemId, Integer> itemLimits = new HashMap<>();

    public static void main(final String[] args) throws Exception {
        itemLimits.put(ItemIdOuterClass.ItemId.ITEM_GREAT_BALL, 100);
        itemLimits.put(ItemIdOuterClass.ItemId.ITEM_POKE_BALL, 100);
        itemLimits.put(ItemIdOuterClass.ItemId.ITEM_POTION, 0);
        itemLimits.put(ItemIdOuterClass.ItemId.ITEM_HYPER_POTION, 0);
        itemLimits.put(ItemIdOuterClass.ItemId.ITEM_SUPER_POTION, 0);
        itemLimits.put(ItemIdOuterClass.ItemId.ITEM_REVIVE, 10);
        new Catchem().run(args);
    }

    public void run(final String[] args) {
        try {
            PokemonGo go = new GoogleAuthenticator().authenticate();

            try {
                displayInventory(go);
            } catch (Exception e) {
                System.err.println("Unable to display inventory: " + e.getMessage());
            }

            // Perth, Australia
            double lat = -32.058087;
            double lon = 115.744325;

            int argIndex = 0;
            if (args.length >= 1) {
                try {
                    // grab initial point from command line or file
                    lat = Double.parseDouble(args[argIndex++]);
                    lon = Double.parseDouble(args[argIndex++]);
                } catch (NumberFormatException e) {
                    // leave argIndex at 0
                    argIndex = 0;
                }
            }

            // randomize starting point
            lat += ((new Random().nextInt(100) + 1 - 50) / 1000000.0);
            lon += ((new Random().nextInt(100) + 1 - 50) / 1000000.0);

            double totalDistance = 0.0;

            // Usage: catchem [lat] [lon] [loot|hunt]
            String algorithm = "loot";

            // argIndex is either 0 or 2
            if (args.length >= argIndex + 1) {
                algorithm = args[argIndex++];
            }

            if ("loot".equals(algorithm)) {
                // establish initial location
                System.out.println("Starting location: " + lat + ", " + lon);
                Point myLocation = setLocation(go, lat, lon);

                // get all pokestops visible from my position
                Collection<Pokestop> stops = go.getMap().getMapObjects().getPokestops();
                System.out.println("Stops visible from this location: " + stops.size());
                List<String> visited = new ArrayList<>();
                int numVisited = 0;

                Pokestop nearest = findNearestStop(myLocation, stops, visited);
                while (nearest != null) {
                    if (Files.deleteIfExists(Paths.get("./_stop_"))) {
                        break;
                    }
                    double distMeters = GeoUtils.distance(myLocation, GeoUtils.pointForStop(nearest), "K") * 1000;
                    System.out.println("Nearest stop is " + distMeters + " meters away.");

                    // walk to the stop
                    Point newLocation = new Walker(go, myLocation).walkTo(nearest);
                    totalDistance += GeoUtils.distance(myLocation, newLocation, "K") * 1000;

                    numVisited++;
                    visited.add(nearest.getId());

                    System.out.println(String.format("%n> Arrived at stop %d:  %s total distance: %f", numVisited, GeoUtils.toString(GeoUtils.pointForStop(nearest)), totalDistance));
                    myLocation = setLocation(go, newLocation.getLatitude(), newLocation.getLongitude());

                    // try to loot
                    for (int tries = 1; tries <= 5; tries++) {
                        try {
                            lootPokeStop(go, nearest);
                            break;
                        } catch (Exception e) {
                            System.out.println("Exception " + e.getMessage() + " looting poke stop.");
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e1) {}
                        }
                    }
                    displayBalls(go);

                    nearest = findNearestStop(myLocation, stops, visited);
                }

                System.out.println("Visited stops:");
                for (Pokestop vStop : stops) {
                    if (visited.contains(vStop.getId())) {
                        System.out.println(GeoUtils.toString(GeoUtils.pointForStop(vStop)));
                    }
                }
            } else if ("hunt".equals(algorithm)) {
                // establish initial location
                System.out.println("Starting location: " + lat + ", " + lon);
                Point myLocation = setLocation(go, lat, lon);

                int numSteps = SPIRAL_STEPS;
                int stepSize = STEP_SIZE;

                if (args.length >= argIndex + 1) {
                    numSteps = Integer.parseInt(args[++argIndex]);
                    stepSize = Integer.parseInt(args[++argIndex]);
                }

                // rectangular spiral search
                double bearings[] = {90.0, 0.0, 270.0, 180.0};
                List<Point> steps = new ArrayList<>();
                steps.add(myLocation);  // add initial location

                for (int step = 1; step <= numSteps; step++) {
                    double bearing = bearings[(step - 1) % 4];
                    int multiplier = (((step - 1) / 2) + 1);
                    Point destination = GeoUtils.destination(myLocation, bearing, multiplier * stepSize);
                    // fudge destination a little
                    destination.setLatitude(destination.getLatitude() + (new Random().nextInt(20) + 1 - 10) / 1000000.0);
                    destination.setLongitude(destination.getLongitude() + (new Random().nextInt(20) + 1 - 10) / 1000000.0);

                    double distMeters = GeoUtils.distance(myLocation, destination, "K") * 1000;
                    System.out.println("next corner is " + distMeters + " meters away.");

                    // walk to the destination
                    Point newLocation = new Walker(go, myLocation).walkTo(destination);
                    steps.add(newLocation);
                    totalDistance += GeoUtils.distance(myLocation, newLocation, "K") * 1000;

                    System.out.println(String.format("> Finished step %d/%d:  %s total distance: %f", step, numSteps, GeoUtils.toString(newLocation), totalDistance));
                    myLocation = setLocation(go, newLocation.getLatitude(), newLocation.getLongitude());
                }

                System.out.println("Spiral steps:");
                for (Point point : steps) {
                    System.out.println(GeoUtils.toString(point));
                }
            } else if ("walk".equals(algorithm)) {
                if (args.length < argIndex + 1) {
                    System.err.println("Usage: walk {filename}");
                    System.exit(-1);
                }

                String filename = args[argIndex++];
                System.out.println("Reading coordinates from: " + filename);
                InputStream is = new FileInputStream(new File(filename));
                List<List<String>> lines = readRecords(new InputStreamReader(is));

                int numPoints = lines.size();
                int pointNum = 0;
                Point myLocation = null;

                for (List<String> line : lines) {
                    Point destination = new Point(Double.valueOf(line.get(0)), Double.valueOf(line.get(1)));
                    pointNum++;
                    if (myLocation == null) {
                        // establish initial location
                        System.out.println("Starting location: " + lat + ", " + lon);
                        myLocation = setLocation(go, destination.getLatitude(), destination.getLongitude());
                    } else {
                        double distMeters = GeoUtils.distance(myLocation, destination, "K") * 1000;
                        System.out.println("next point is " + distMeters + " meters away.");

                        // walk to the destination
                        Point newLocation = new Walker(go, myLocation).walkTo(destination);
                        totalDistance += GeoUtils.distance(myLocation, newLocation, "K") * 1000;
                        System.out.println(String.format("%n> Arrived at point %d/%d, total distance: %f", pointNum, numPoints, totalDistance));
                    }
                }
            } else if ("transfer".equals(algorithm)) {
                if (args.length < argIndex + 2) {
                    System.err.println("Usage: transfer {name} {max-level-to-transfer}");
                    System.exit(-1);
                }

                String name = args[argIndex++];
                int maxLevel = Integer.valueOf(args[argIndex++]);
                System.out.println("name: " + name + " maxLevel: " + maxLevel);

                List<Pokemon> pokemons = go.getInventories().getPokebank().getPokemons();
                List<Pokemon> transfer = new ArrayList<>();

                for (Pokemon pokemon : pokemons) {
                    if (pokemon.getPokemonId().name().equalsIgnoreCase(name) && pokemon.getCp() <= maxLevel) {
                        transfer.add(pokemon);
                    }
                }

                for (Pokemon pokemon : transfer) {
                    System.out.println("Transferring " + pokemon.getPokemonId().name() + " (" + pokemon.getCp() +
                            ")... " + pokemon.transferPokemon());
                }
            } else if ("distance".equals(algorithm)) {
                if (args.length < argIndex + 4) {
                    System.err.println("Usage: distance {NW lat} {NW lon} {SE lat} {SE lon}");
                    System.exit(-1);
                }

                double Lat1 = Double.valueOf(args[argIndex++]);
                double Lon1 = Double.valueOf(args[argIndex++]);
                double Lat2 = Double.valueOf(args[argIndex++]);
                double Lon2 = Double.valueOf(args[argIndex++]);

                double distanceDiag = GeoUtils.distance(new Point(Lat1, Lon1), new Point(Lat2, Lon2), "K") * 1000;
                System.out.println(String.format("Distance in meters: %f", distanceDiag));
            } else if ("radar".equals(algorithm)) {
                if (args.length < argIndex + 4) {
                    System.err.println("Usage: radar {NW lat} {NW lon} {SE lat} {SE lon}");
                    System.exit(-1);
                }

                double NWLat = Double.valueOf(args[argIndex++]);
                double NWLon = Double.valueOf(args[argIndex++]);
                double SELat = Double.valueOf(args[argIndex++]);
                double SELon = Double.valueOf(args[argIndex++]);

                double distanceDiag = GeoUtils.distance(new Point(NWLat, NWLon), new Point(SELat, SELon), "K") * 1000;
                System.out.println(String.format("Diagonal distance in meters: %f", distanceDiag));

                int numRows = calculateRows(NWLat, NWLon, SELat, TELEPORT_STEP);
                int numCols = calculateColumns(NWLat, NWLon, SELon, TELEPORT_STEP);
                System.out.println("Grid Size: " + numRows + " x " + numCols);

                for (int row = 0; row < numRows; row++) {
                    for (int col = 0; col < numCols; col++) {
                        Point position = calculateGridPoint(NWLat, NWLon, row, col, TELEPORT_STEP);
                        position = setLocation(go, position.getLatitude(), position.getLongitude());

                        // System.out.println(GeoUtils.toString(position));
                        new Walker(go, position).visit();
                    }
                }

                int counter = new Walker(go, new Point(NWLat, NWLon)).getCounter();
                System.out.println("Encountered " + counter);
            } else {
                System.out.println("Usage: catchem [{lat} {lon}] [loot|hunt|walk|transfer] [[numSteps] [stepSize] | filename | [name] [maxLevel]].");
                System.exit(0);
            }
        }
        catch (LoginFailedException | RemoteServerException | IOException e) {
            e.printStackTrace();
        } catch (NoSuchItemException e) {
            e.printStackTrace();
        }
        System.out.println(Calendar.getInstance().getTime());
        System.exit(0);
    }

    /**
     * Given two corners of a geographical area, calculate how many "rows" of stepSize it covers
     */
    private int calculateRows(double nwLat, double nwLon, double seLat, int stepSize) {
        double distance = GeoUtils.distance(new Point(nwLat, nwLon), new Point(seLat, nwLon), "K") * 1000;
        return (int) (distance / stepSize);
    }

    /**
     * Given two corners of a geographical area, calculate how many "columns" of stepSize it covers
     */
    private int calculateColumns(double nwLat, double nwLon, double seLon, int stepSize) {
        double distance = GeoUtils.distance(new Point(nwLat, nwLon), new Point(nwLat, seLon), "K") * 1000;
        return (int) (distance / stepSize);
    }

    /**
     * Given the start corner and a row and column, calculate the location on the grid using stepSize cells.
     */
    private Point calculateGridPoint(double nwLat, double nwLon, int row, int col, int stepSize) {
        Point origin = new Point(nwLat, nwLon);
        Point moveDown = GeoUtils.destination(origin, 180.0, stepSize * row);
        Point moveRight = GeoUtils.destination(moveDown, 90.0, stepSize * col);

        return moveRight;
    }

    Point setLocation(PokemonGo go, double lat, double lon) {
        go.setLocation(lat, lon, 0);
        return new Point(lat, lon);
    }

    Pokestop findNearestStop(Point location, Collection<Pokestop> stops, List<String> visited) {
        double min = 1000.0;
        Pokestop nearest = null;

        for (Pokestop stop : stops) {
            if (visited.contains(stop.getId())) {
                continue;
            }
            double distance = GeoUtils.distance(location, GeoUtils.pointForStop(stop), "K");
            if (distance < min) {
                min = distance;
                nearest = stop;
            }
        }

        return nearest;
    }

    private void lootPokeStop(PokemonGo go, Pokestop nearest) throws LoginFailedException, RemoteServerException {
        if (nearest.inRange() && nearest.canLoot()) {
            System.out.println("Attempting to loot...");
            PokestopLootResult result = nearest.loot();
            System.out.println("Attempt to loot: " + result.getResult());
            if (result.wasSuccessful()) {
                // reduce inventory levels
                try {
                    go.getInventories().updateInventories();
                    for (Item item : go.getInventories().getItemBag().getItems()) {
                        if (itemLimits.containsKey(item.getItemId())) {
                            int limit = itemLimits.get(item.getItemId());
                            if (item.getCount() > limit) {
                                go.getInventories().getItemBag().removeItem(item.getItemId(), item.getCount() - limit);
                            }
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Error " + e.getMessage() + " during updateInventories()");
                }
            }
        }
    }

    private void displayBalls(PokemonGo go) throws LoginFailedException, RemoteServerException {
        ItemBag bag = go.getInventories().getItemBag();
        int numRegular = bag.getItem(ItemIdOuterClass.ItemId.ITEM_POKE_BALL).getCount();
        int numGreat = bag.getItem(ItemIdOuterClass.ItemId.ITEM_GREAT_BALL).getCount();
        System.out.println(String.format("Pokeballs: %d Greatballs: %d", numRegular, numGreat));
    }

    private void displayInventory(PokemonGo go) throws InvalidCurrencyException, LoginFailedException,
            RemoteServerException {
        // After this you can access the api from the PokemonGo instance :
        PlayerProfile profile = go.getPlayerProfile();
        int stardust = profile.getCurrency(PlayerProfile.Currency.STARDUST);
        int pokecoins = profile.getCurrency(PlayerProfile.Currency.POKECOIN);
        System.out.println("stardust: " + stardust + " coins: " + pokecoins);
        Inventories inventories = go.getInventories();
        System.out.println("Inventory size: " + inventories.getItemBag().getItems().size());
        for (Item item : inventories.getItemBag().getItems()) {
            System.out.println("item: " + item + " id: " + item.getItemId() + " count: " + item.getCount());
        }
    }

    static List<List<String>> readRecords(Reader source) {
        List<List<String>> lines = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(source)) {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(Arrays.asList(line.split(",\\s*")));
            }
            return lines;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
