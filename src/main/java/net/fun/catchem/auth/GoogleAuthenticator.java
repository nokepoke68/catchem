package net.fun.catchem.auth;

import com.pokegoapi.api.PokemonGo;
import com.pokegoapi.auth.GoogleUserCredentialProvider;
import com.pokegoapi.exceptions.LoginFailedException;
import com.pokegoapi.exceptions.RemoteServerException;
import okhttp3.OkHttpClient;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Scanner;

import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Paths.get;

public class GoogleAuthenticator {

    public static final String REFRESH_TOKEN = "_refreshToken_";

    public PokemonGo authenticate() throws LoginFailedException, RemoteServerException {
        OkHttpClient httpClient = new OkHttpClient();
        PokemonGo go = null;

        File token = new File(REFRESH_TOKEN);
        try {
            if (token.exists()) {
                /**
                 * After this, if you do not want to re-authorize the google account every time,
                 * you will need to store the refresh_token that you can get the first time with provider.getRefreshToken()
                 * ! The API does not store the refresh token for you !
                 * log in using the refresh token like this :
                 */
                String refreshToken = new String(readAllBytes(get(REFRESH_TOKEN)));
                go = new PokemonGo(httpClient);
                go.login(new GoogleUserCredentialProvider(httpClient, refreshToken));
            }
        } catch (IOException e) {
            System.err.println(e);
        }

        if (go == null) {
            /**
             * Google:
             * You will need to redirect your user to GoogleUserCredentialProvider.LOGIN_URL
             * Afer this, the user must signin on google and get the token that will be show to him.
             * This token will need to be put as argument to login.
             */
            GoogleUserCredentialProvider provider = new GoogleUserCredentialProvider(httpClient);

            // in this url, you will get a code for the google account that is logged
            System.out.println("Please go to " + GoogleUserCredentialProvider.LOGIN_URL);
            System.out.println("Enter authorization code:");

            // Ask the user to enter it in the standart input
            Scanner sc = new Scanner(System.in);
            String access = sc.nextLine();

            // we should be able to login with this token
            provider.login(access);
            go = new PokemonGo(httpClient);
            go.login(provider);

            String refreshToken = provider.getRefreshToken();
            System.out.println("token: [" + refreshToken + "]");
            try {
                Files.write(get(REFRESH_TOKEN), refreshToken.getBytes());
            } catch (IOException e) {
                System.err.println(e);
            }
        }

        if (go == null) {
            System.err.println("Unable to login to Google...");
            throw new RuntimeException("Unable to authenticate with Google...");
        }

        /**
         * PTC is much simpler, but less secure.
         * You will need the username and password for each user log in
         * This account does not currently support a refresh_token.
         * Example log in :
         */
        // go = new PokemonGo(new PtcCredentialProvider(httpClient,username,password),httpClient);

        return go;
    }
}
