package net.fun.catchem.geo;

import com.pokegoapi.api.map.Point;
import com.pokegoapi.api.map.fort.Pokestop;

import java.util.Objects;

public class GeoUtils {

    /**
     * calculate the distance between two points
     *     var φ1 = lat1.toRadians(), φ2 = lat2.toRadians(), Δλ = (lon2-lon1).toRadians(), R = 6371e3; // gives d in metres
     *     var d = Math.acos( Math.sin(φ1)*Math.sin(φ2) + Math.cos(φ1)*Math.cos(φ2) * Math.cos(Δλ) ) * R;
     */
    public static double distance(Point pt1, Point pt2, String unit) {
        double φ1 = deg2rad(pt1.getLatitude());
        double λ1 = deg2rad(pt1.getLongitude());
        double φ2 = deg2rad(pt2.getLatitude());
        double λ2 = deg2rad(pt2.getLongitude());
        double Δλ = λ2 - λ1;

        double dist = Math.sin(φ1) * Math.sin(φ2) + Math.cos(φ1) * Math.cos(φ2) * Math.cos(Δλ);
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (Objects.equals(unit, "K")) {
            dist = dist * 1.609344;
        } else if (Objects.equals(unit, "N")) {
            dist = dist * 0.8684;
        }

        return (dist);
    }

    /**
     * Formula: 	θ = atan2( sin Δλ ⋅ cos φ2 , cos φ1 ⋅ sin φ2 − sin φ1 ⋅ cos φ2 ⋅ cos Δλ )
     * where 	φ1,λ1 is the start point, φ2,λ2 the end point (Δλ is the difference in longitude)
     *
     * from: http://www.movable-type.co.uk/scripts/latlong.html
     *
     * Since atan2 returns values in the range -π ... +π (that is, -180° ... +180°),
     * to normalise the result to a compass bearing (in the range 0° ... 360°,
     * with −ve values transformed into the range 180° ... 360°), convert to degrees and then use (θ+360) % 360, where % is (floating point) modulo.
     */
    public static double bearing(Point pt1, Point pt2) {
        double φ1 = deg2rad(pt1.getLatitude());
        double λ1 = deg2rad(pt1.getLongitude());
        double φ2 = deg2rad(pt2.getLatitude());
        double λ2 = deg2rad(pt2.getLongitude());
        double Δλ = λ2 - λ1;

        double y = Math.sin(Δλ) * Math.cos(φ2);
        double x = Math.cos(φ1) * Math.sin(φ2) - Math.sin(φ1) * Math.cos(φ2) * Math.cos(Δλ);

        double bearing = rad2deg(Math.atan2(y, x));
        bearing = (bearing + 360) % 360;

        return bearing;
    }

    /**
     * Formula:
     *  φ2 = asin( sin φ1 ⋅ cos δ + cos φ1 ⋅ sin δ ⋅ cos θ )
     *  λ2 = λ1 + atan2( sin θ ⋅ sin δ ⋅ cos φ1, cos δ − sin φ1 ⋅ sin φ2 )
     *
     *  where 	φ is latitude, λ is longitude, θ is the bearing (clockwise from north),
     *  δ is the angular distance d/R; d being the distance travelled, R the earth’s radius
     *
     * from: http://www.movable-type.co.uk/scripts/latlong.html
     *
     *      The longitude can be normalised to −180…+180 using (lon+540)%360-180
     */
    public static Point destination(Point origin, double bearing, double distance) {
        double φ1 = deg2rad(origin.getLatitude());
        double λ1 = deg2rad(origin.getLongitude());
        double R = 6371e3; // Earth's radius in meters
        double brng = deg2rad(bearing);

        double φ2 = Math.asin(Math.sin(φ1) * Math.cos(distance / R) + Math.cos(φ1) * Math.sin(distance / R) *
                Math.cos(brng));
        double λ2 = λ1 + Math.atan2(Math.sin(brng) * Math.sin(distance / R) * Math.cos(φ1),
                Math.cos(distance / R) - Math.sin(φ1) * Math.sin(φ2));
        λ2 = (λ2 + 540) % 360 - 180;    // normalize

        return new Point(rad2deg(φ2), rad2deg(λ2));
    }

    /**
     * Return the point which would result from heading from origin toward destination at a speed represented by delta
     */
    public static Point headToward(Point origin, Point destination, double delta) {
        return new Point(1.0, 1.0);
    }

    /**
     * convert decimal degrees to radians
     */
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /**
     * convert radians to decimal degrees
     */
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    public static Point pointForStop(Pokestop stop) {
        return new Point(stop.getLatitude(), stop.getLongitude());
    }

    public static String toString(Point p) {
        return String.format("%f, %f", p.getLatitude(), p.getLongitude());
    }
}
