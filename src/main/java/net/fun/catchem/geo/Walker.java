package net.fun.catchem.geo;

import POGOProtos.Enums.PokemonIdOuterClass;
import POGOProtos.Inventory.Item.ItemIdOuterClass;
import POGOProtos.Networking.Responses.CatchPokemonResponseOuterClass;
import POGOProtos.Networking.Responses.ReleasePokemonResponseOuterClass;
import com.pokegoapi.api.PokemonGo;
import com.pokegoapi.api.inventory.ItemBag;
import com.pokegoapi.api.map.Point;
import com.pokegoapi.api.map.fort.Pokestop;
import com.pokegoapi.api.map.pokemon.CatchResult;
import com.pokegoapi.api.map.pokemon.CatchablePokemon;
import com.pokegoapi.api.map.pokemon.NearbyPokemon;
import com.pokegoapi.api.map.pokemon.encounter.EncounterResult;
import com.pokegoapi.api.pokemon.Pokemon;
import com.pokegoapi.api.settings.CatchOptions;
import com.pokegoapi.exceptions.AsyncPokemonGoException;
import com.pokegoapi.exceptions.LoginFailedException;
import com.pokegoapi.exceptions.NoSuchItemException;
import com.pokegoapi.exceptions.RemoteServerException;

import java.util.List;

public class Walker {
    private static final int FREQUENCY = 2;  // in seconds
    private static final double METERS_PER_SECOND = 1.4;
    private static final int BALL_LIMIT = 7;
    private static final String NO_HUNT = "noHunt";

    private PokemonGo go;
    private Point location;
    private static int counter = 0;

    public Walker(PokemonGo go, Point location) {
        this.go = go;
        this.location = location;
    }

    public int getCounter() {
        return counter;
    }

    public Point walkTo(Pokestop stop) throws LoginFailedException, RemoteServerException, NoSuchItemException {
        Point stopLocation = new Point(stop.getLatitude(), stop.getLongitude());
        //double distanceInMeters = GeoUtils.distance(location, stopLocation, "K");
        double bearing = GeoUtils.bearing(location, stopLocation);

        while (!stop.inRange()) {
            try {
                // look for Pokemon in the area
                hunt();

                Thread.sleep(1000 * FREQUENCY);
            } catch (InterruptedException e) {
            } catch (RemoteServerException re) {
                System.out.println(re);
            } catch (AsyncPokemonGoException apge) {
                System.out.println(apge.getMessage());
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {}
            }

            // move FREQUENCY seconds worth in the direction of the stop
            location = GeoUtils.destination(location, bearing, FREQUENCY * METERS_PER_SECOND);
            System.out.print("w");
            go.setLocation(location.getLatitude(), location.getLongitude(), 0.0);
        }

        return location;
    }

    public Point walkTo(Point target) throws LoginFailedException, RemoteServerException, NoSuchItemException {
        double bearing = GeoUtils.bearing(location, target);

        // find number of "steps" needed to cover the distance
        int numSteps = calculateSteps(location, target, FREQUENCY * METERS_PER_SECOND);
        for (int step = 1; step <= numSteps; step++) {
            try {
                // look for Pokemon in the area
                hunt();

                Thread.sleep(1000 * FREQUENCY);
            } catch (InterruptedException e) {
            } catch (AsyncPokemonGoException | RemoteServerException ex) {
                System.out.println(ex.getMessage());
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {}
            }

            // move FREQUENCY seconds worth in the direction of the stop
            location = GeoUtils.destination(location, bearing, FREQUENCY * METERS_PER_SECOND);
            System.out.print("w");
            go.setLocation(location.getLatitude(), location.getLongitude(), 0.0);
        }

        return location;
    }

    private int calculateSteps(Point location, Point target, double stepDistance) {
        double distance = GeoUtils.distance(location, target, "K") * 1000;
        return (int) (distance / stepDistance) + 1;  // go slightly over rather than less
    }

    private void hunt() throws LoginFailedException, RemoteServerException, NoSuchItemException {
        // skip hunting if system property 'noHunt' set to true
        if (Boolean.valueOf(System.getProperty(NO_HUNT))) {
            return;
        }

        List<CatchablePokemon> catchablePokemon = go.getMap().getCatchablePokemon();

        if (catchablePokemon.size() > 0) {
            System.out.println("Pokemon in area:" + catchablePokemon.size());
        }

        boolean failed = false;
        while (catchablePokemon.size() > 0) {
            System.out.println("catchablePokemon size: " + catchablePokemon.size());
            for (CatchablePokemon cp : catchablePokemon) {
                System.out.print(">");
                try {
                    // You need to Encounter first.
                    EncounterResult encResult = cp.encounterPokemon();
                    // if encounter was successful, catch
                    if (encResult.wasSuccessful()) {
                        System.out.println("Encountered:" + cp.getPokemonId() + " cp: " + encResult.getPokemonData().getCp() + " probability: " + encResult.getCaptureProbability().getCaptureProbability(0));
                        CatchResult result = catchIt(encResult, cp);
                        System.out.println("Attempt to catch:" + cp.getPokemonId() + " " + result.getStatus());
                        // fail when CATCH_ERROR
                        if (result.getStatus() == CatchPokemonResponseOuterClass.CatchPokemonResponse.CatchStatus.CATCH_ERROR) {
                            System.out.println("Failure... " + result.getStatus());
                            throw new RemoteServerException("Catch Error");
                        }
                        transferPests(cp.getPokemonId());

                    } else {
                        failed = true;
                        System.out.println(encResult.getStatus());
                        break;
                    }
                } catch (Exception e) {
                    System.out.print("E: " + e.getMessage());
                    e.printStackTrace();
                    failed = true;
//                    throw e;
                }
            }
            // return from hunt() in case of failure
            if (failed) {
                break;
            }
            go.getMap().clearCache();
            System.out.println("getting pokemon...");
            catchablePokemon = go.getMap().getCatchablePokemon();
        }
    }

    private CatchResult catchIt(EncounterResult encResult, CatchablePokemon pokemon) throws LoginFailedException, RemoteServerException, NoSuchItemException {
        float probability = encResult.getCaptureProbability().getCaptureProbability(0); // normal ball probability
        int cp = encResult.getPokemonData().getCp();
//        Pokeball ballType = Pokeball.POKEBALL;
//        int berryLimit = 0;

        CatchOptions catchOptions = new CatchOptions(go);

        // use berries for anything higher than cp 400
        if (cp > 400) {
            catchOptions.useRazzberry(true);
            catchOptions.maxRazzberries(-1);
            //berryLimit = -1;
        } else {
            catchOptions.useRazzberry(false);
        }

        // see if we should use a berry and great ball
        if ((probability < 0.40 && cp > 600) || pokemon.getPokemonId() == PokemonIdOuterClass.PokemonId.SNORLAX || pokemon.getPokemonId() == PokemonIdOuterClass.PokemonId.LAPRAS) {
            catchOptions.useBestBall(true);
        } else {
            catchOptions.useSmartSelect(true);
        }

/*
        if (ballType == Pokeball.POKEBALL && ballCount == 0) {
            if (greatCount > 0) {
                ballType = Pokeball.GREATBALL;
                System.out.println("Overriding ball type to " + ballType);
            } else if (ultraCount > 0) {
                ballType = Pokeball.ULTRABALL;
                System.out.println("Overriding ball type to " + ballType);
            } else {
                System.out.println("NO BALLS!");
            }
        }
*/

        // catch
//        CatchResult result = pokemon.catchPokemon(1.0D, 1.95D + Math.random() * 0.05D, 0.85D + Math.random() * 0.15D, ballType, BALL_LIMIT, berryLimit);
        System.out.println("attempting catch...");
        CatchResult result = pokemon.catchPokemon(catchOptions);
//        System.out.println("result: " + result);
        if (result.getStatus() != CatchPokemonResponseOuterClass.CatchPokemonResponse.CatchStatus.CATCH_SUCCESS) {
            ItemBag bag = go.getInventories().getItemBag();
            int ballCount = bag.getItem(ItemIdOuterClass.ItemId.ITEM_POKE_BALL).getCount();
            int greatCount = bag.getItem(ItemIdOuterClass.ItemId.ITEM_GREAT_BALL).getCount();
            int ultraCount = bag.getItem(ItemIdOuterClass.ItemId.ITEM_ULTRA_BALL).getCount();
            System.out.println(String.format("Failed with %s Counts:  %d:%d:%d", catchOptions.getItemBall(), ballCount, greatCount, ultraCount));
        }

        return result;
    }

    public void visit() throws LoginFailedException, RemoteServerException {
        try {
            Thread.sleep(10000 * FREQUENCY); // just a test
        } catch (InterruptedException e) {
        }
        List<NearbyPokemon> nearbyPokemon = go.getMap().getNearbyPokemon();
        if (nearbyPokemon.size() > 0) {
            System.out.println("Found " + nearbyPokemon.size() + " nearby pokemon.");
            counter += nearbyPokemon.size();
            for (NearbyPokemon cp : nearbyPokemon) {
                PokemonIdOuterClass.PokemonId id = cp.getPokemonId();
                if (letMeKnowNow(id)) {
                    throw new RuntimeException(String.format("FOUND [%s] at %f %f !!!", id, location.getLatitude(), location.getLongitude()));
                }
            }
        }
    }

    private void transferPests(PokemonIdOuterClass.PokemonId pokemonId) throws LoginFailedException, RemoteServerException {
        if (!isPest(pokemonId)) {
            return;
        }

        List<Pokemon> pests = go.getInventories().getPokebank().getPokemonByPokemonId(pokemonId);

        for (Pokemon pest : pests) {
            // don't release high cp pests
            if (pest.getCp() <= 360) {
                ReleasePokemonResponseOuterClass.ReleasePokemonResponse.Result result = pest.transferPokemon();
                System.out.println("transfer " + pokemonId + " result: " + result);
            }
        }
    }

    private boolean isPest(PokemonIdOuterClass.PokemonId pokemonId) {
        return (pokemonId == PokemonIdOuterClass.PokemonId.PIDGEY ||
                pokemonId == PokemonIdOuterClass.PokemonId.RATTATA ||
                pokemonId == PokemonIdOuterClass.PokemonId.PARAS ||
                pokemonId == PokemonIdOuterClass.PokemonId.ZUBAT);
    }

    private boolean letMeKnowNow(PokemonIdOuterClass.PokemonId id) {
        return (id == PokemonIdOuterClass.PokemonId.AERODACTYL
                || id == PokemonIdOuterClass.PokemonId.CHARMELEON
                || id == PokemonIdOuterClass.PokemonId.DRAGONAIR
                || id == PokemonIdOuterClass.PokemonId.DRAGONITE
                || id == PokemonIdOuterClass.PokemonId.GYARADOS
                || id == PokemonIdOuterClass.PokemonId.IVYSAUR
                || id == PokemonIdOuterClass.PokemonId.LAPRAS
                || id == PokemonIdOuterClass.PokemonId.SNORLAX);
    }
}
