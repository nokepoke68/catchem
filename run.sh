#!/bin/bash
LIBS="libs/PokeGOAPI-library-0.4.1.jar:libs/okhttp-3.4.1.jar:libs/okio-1.9.0.jar:libs/moshi-1.2.0.jar:libs/protobuf-java-3.0.0-beta-3.jar"
LIBS="$LIBS:libs/rxjava-1.0.12.jar:libs/stream-1.1.1.jar:libs/lz4-1.3.0.jar:target/catchem-1.0-SNAPSHOT.jar"
echo LIBS: $LIBS
java -DnoHunt=true -cp $LIBS net.fun.catchem.cli.Catchem $@
#java -cp $LIBS net.fun.catchem.cli.Catchem $@
